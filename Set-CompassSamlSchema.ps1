<#
.SYNOPSIS
    Get all groups in an OU and creates Custom GSuite Attributes based on those group names
.EXAMPLE
    The group "CompassSponsors" will create a custom schema & attribute as:
    Compass.groups-CompassSponsors
#>


$CompassGroups_OU = "OU=Compass,OU=Groups,OU=McKinnon,DC=noddy,DC=mckinnonsc,DC=vic,DC=edu,DC=au"
$CompassGroups = (Get-ADGroup -SearchBase $CompassGroups_OU -Filter *).Name

$SchemaName = "Compass"
gam info schema $SchemaName 2>&1 | Out-Null

If($?) {
    $CompassGroups | ForEach-Object {
        gam update schema $SchemaName field groups-$_ type string restricted endfield 2>&1 | Out-Null
        if(!$?) {Write-Host "Failed to update schema: " + $Error[0]}
    }
} else {
    Write-Host "Failed to get schema:" + $Error[0]
}