<#
.SYNOPSIS
    A collection of functions to get all groups memberships from an OU and assign
    custom attributes based on those group names with a specified prefix
.EXAMPLE
    See below comment "# Go time!" for a working example
    All members of groups in "OU=Compass" will be assigned custom GSuite variables such as:
    groups-CompassSponsors for all users in the AD Group "CompassSponsors" where "groups-"
#>


$CompassGroups_OU = "OU=Compass,OU=Groups,OU=McKinnon,DC=noddy,DC=mckinnonsc,DC=vic,DC=edu,DC=au"
$CompassGroups = (Get-ADGroup -SearchBase $CompassGroups_OU -Filter *)
$BatchCsv = "C:\ScheduledTasks\CompassSamlGSuite\batch.csv"


function Get-CompassGroupUsers {
# Takes a list of groups and returns a list of users and their groups
# eg. samn {"CompassSponsors", "CompassTechnicians"}
    param(
        [Parameter(Mandatory=$true)]
        [array]$Groups
    )
    $UserGroupTable = @{}
    $CompassGroups | ForEach-Object {
        $Group = $_
        (Get-ADGroupMember -Identity $_ -Recursive).SamAccountName | ForEach-Object {
            $User = $_
            [PSCustomObject]@{
                User = $User;
                Group = $Group
            }
        }
    } | ForEach-Object {
        If($User -and $Group) {
            $UserGroupTable.$User += @($Group)
        }
    }
    return $UserGroupTable
}

function Get-GoogleCompassPermissions {
    $CompassUserPermissions = @{}
    $GoogleUsers = gam print users custom Compass query "isSuspended=False" | ConvertFrom-Csv
    $GoogleUsers | ForEach-Object {
        $Perms = $($_.PSObject.Properties.Value | Where-Object { $_ -Like "Compass*" -and -not [string]::IsNullOrEmpty($_) }) -join ","
        If($Perms) {
            $CompassUserPermissions.Add($_.primaryEmail, $Perms)
        }
    }
    $CompassUserPermissions
}

# Go time!
if(Test-Path $BatchCsv) {
    Clear-Content $BatchCsv
}

# Get and clear all set schema values in Google Workspace
$GooglePermissions = Get-GoogleCompassPermissions
$GooglePermissions.GetEnumerator() | ForEach-Object {
    "gam update user {0} clearschema Compass" -f $_.Name.Trim() | Add-Content $BatchCsv
}


$ADPermissions = Get-CompassGroupUsers -Groups $CompassGroups
$ADPermissions.GetEnumerator() | ForEach-Object {
    $Email = "{0}@mckinnonsc.vic.edu.au" -f $_.Name.Trim()
    $_.Value | ForEach-Object {
        "gam update user {0} Compass.groups-{1} {1}" -f $Email, $_.Name.Trim() | Add-Content $BatchCsv
    }
}

gam batch $BatchCsv

Invoke-RestMethod https://hc-ping.com/a7286f22-83a5-44c4-852c-da73937dda5c
