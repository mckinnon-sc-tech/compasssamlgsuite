# CompassSamlGSuite

Just a small set of tools to assist in pushing group memberships from Active Directory to GSuite custom attributes.  
These attributes can be used for SAML Group Assertions for Compass.